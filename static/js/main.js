
var APP = {

	TIME: 5,
	CLICK: 0,
	VOUCHER_SERVICE: 'https://voucher.sa-hackathon-13.cluster.extend.sap.cx/voucher',

	GO_CLASS: 'go',

	clickNumberDisplayElement: document.querySelector('#mycount'),
	timeNumberDisplayElement: document.querySelector('#timecount'),
	beginInDisplayElement: document.querySelector('#beginIn'),
	startButtonElement: document.querySelector('#startButton'),
	clickButtonElement: document.querySelector('#clickButton'),
	voucher: document.querySelector('#voucher'),
	voucherBackground: document.querySelector('#voucherBackground'),
	voucherCode: document.querySelector('.voucher-code'),
	voucherDiscount: document.querySelector('.voucher-discount'),
	hideButton: document.querySelector('.x-button'),

	interval: {},

	displayClickNumber: function(){
		this.clickNumberDisplayElement.innerHTML = this.CLICK;
	},

	displayTime: function(){
		this.timeNumberDisplayElement.innerHTML = this.TIME;
	},

	attachStartButton: function(){
		this.startButtonElement.onclick = function(){
			APP.setGoClass();
			APP.displayStartIn();
			// clear
			APP.startButtonElement.onclick = function(){};
		}
	},

	displayStartIn: function(){
		var startIn = 3;
		var interval = setInterval(function(){
			APP.startButtonElement.innerHTML = startIn;
			startIn--;
			if(startIn < 0){
				clearInterval(interval);
				APP.startButtonElement.innerHTML = 'GO!';
				APP.startInterval();
				APP.displayTime();
			}
		}, 1000);
	},

	startInterval: function(){
		this.attachClick();
		this.interval = setInterval(function(){
			APP.TIME--;
			APP.displayTime();

			if(APP.TIME <= 0){
				APP.stopInterval();
				APP.detachClick();
				APP.displayVoucher();
			}

		}, 1000);
	},

	stopInterval: function(){
		clearInterval(APP.interval);
	},

	attachClick: function(){
		this.clickButtonElement.onclick = function(){
			APP.CLICK++;
			APP.displayClickNumber();
			APP.displayValue();
		}
	},

	displayValue: function(){
		let cls = 'levels show0';

		if(APP.CLICK >=1 && APP.CLICK <= 10) {
			cls = 'levels show10';
		} else if(APP.CLICK >= 11 && APP.CLICK <=20) {
			cls = 'levels show15';
		}else if(APP.CLICK > 20){
			cls = 'levels show20';
		}

		document.querySelector('.levels').className = cls;
	},

	detachClick: function(){
		this.clickButtonElement.onclick = function(){};
	},

	getSegment: function(){
		let segment = 'CLICK10';
		
		if(APP.CLICK >= 11 && APP.CLICK <=20) {
			segment = 'CLICK15';
		}else if(APP.CLICK > 20){
			segment = 'CLICK20';
		}

		return segment;
	},

	displayVoucher: function(){

		if(APP.CLICK == 0) {

			this.voucherCode.innerHTML = '';
			this.voucherDiscount.innerHTML = 'Lazy Bastard!';
			this.voucher.classList.remove('hide');
			this.voucherBackground.classList.remove('hide');

			return;
		}

		const segment = APP.getSegment();

		fetch(`${APP.VOUCHER_SERVICE}/${segment}`)
		.then(response => response.json())
		.then(function(data){
			console.log('data >>', data);
			APP.showVoucher(data);
		})
		.catch(function(err){
			console.log('err', err);
		})
	},

	showVoucher: function(data) {
		this.voucherCode.innerHTML = data.code;
		this.voucherDiscount.innerHTML = APP.prepareDiscount(data.group);
		this.voucher.classList.remove('hide');
		this.voucherBackground.classList.remove('hide');

	},

	prepareDiscount: function(code) {
		switch (code) {
			case 'CLICK10': 
				return '-10%';
			case 'CLICK15':
				return '-15%';
			case 'CLICK20':
				return '-20%';
		}
	},

	setGoClass: function() {
		console.log(document.querySelector('body').classList.add('go'));
	},

	prepareView: function(){
		this.displayClickNumber();
		this.attachStartButton();
	}

};

APP.prepareView();