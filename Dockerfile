# Which Docker image shall we based ours upon?
FROM golang
# Do we want to copy over any files ?
ADD . app/

# When it runs, which working directory should the Container use
WORKDIR app/

# What command should the running Docker Container execute once it is ready?
CMD go run main.go
